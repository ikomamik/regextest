require 'spec_helper'

describe Regextest do
  it 'can parse look-ahead and generate sample data' do
    expect(/(?=aa)./.sample).to eq("aa")
  end

  it 'can parse concurrent look-ahead and generate sample data' do
    expect(/(?=aa)(?!\w\d)./.sample).to eq("aa")
  end

  it 'can parse concurrent look-ahead and generate sample data' do
    expect(/a(?=aa)(?!\w\d)/.sample).to eq("aaa")
  end

  it 'can parse nested look-ahead and generate sample data' do
    expect(/(?=aa(?=\wa))..a/.sample).to eq("aaaa")
  end

  it 'can parse nested look-ahead/look-behind and generate sample data' do
    expect(/(?=aa(?<=\wa))..a/.sample).to eq("aaa")
  end

  it 'can parse atomic selection and generate sample data' do
    expect(/(?>ab|a|b){4}/.sample).to be_a(String)
  end

  it 'can parse atomic selection and generate sample data' do
    expect(/(?=c)(?>ab|a|c)/.sample).to be_a(String)
  end

  it 'can parse reluctant repeat and generate sample data' do
    expect(/\w{2}?\d/.sample).to match /[a-zA-Z_]{2}\d/
  end

  it 'can parse reluctant repeat and generate sample data' do
    expect(/(ab|cd){2}?bc/.sample).to be_a String
  end

  it 'can parse possessive repeat and generate sample data' do
    expect(/a++[ab]/.sample).to match /a+b/
  end

  it 'can parse possessive repeat and generate sample data' do
    expect(/[ab]++[abc]/.sample).to match /a+b/
  end

  it 'can parse look-ahead-negative + select and generate sample data' do
    expect(/(?![a-zA-Z]|[1-9])\h/.sample).to eq("0")
  end

  it 'can parse nested look-ahead-negative + select and generate sample data' do
    expect(/(?![abc]|(d|e))[a-f]/.sample).to eq("f")
  end

  it 'can generate sample string of /\A\bword\b\z/' do
    expect(/\A\bword\b\z/.sample).to eq("word")
  end

  it 'can generate sample string of /\b^word\Z\b/' do
    expect(/\b^word\Z\b/.sample).to eq("word")
  end

  it 'cannot generate sample string of /\b^###/' do
    expect{/\b^###/.sample}.to raise_error(Regextest::RegextestError)
  end

  it 'cannot generate sample string of /###\b\Z\z/' do
    expect{/###\b\Z\z/.sample}.to raise_error(Regextest::RegextestError)
  end

  it 'cannot generate sample string of /\A\Bword/' do
    expect{/\A\Bword/.sample}.to raise_error(Regextest::RegextestError)
  end

  it 'cannot generate sample string of /\B^word/' do
    expect{/\B^word/.sample}.to raise_error(Regextest::RegextestError)
  end

  it 'can generate sample string of /\A\B###\B\z/' do
    expect(/\A\B###/.sample).to eq("###")
  end

  it 'can generate sample string of /\B^###\Z$\B/' do
    expect(/\B^###\Z$\B/.sample).to eq("###")
  end

  it 'can generate sample string of /(^a)\1/' do
    expect(/(^a)\1/.sample).to eq("aa")
  end

  it 'can generate sample string of /(a\b)=\1a/' do
    expect(/(a\b)=\1a/.sample).to eq("a=aa")
  end

  it 'cannot generate sample string of /(^a){2}/' do
    expect{/(^a){2}/.sample}.to raise_error(Regextest::RegextestError)
  end

  it 'can generate sample string of /[^[^あ]]/' do
    expect(/[^[^あ]]/.sample).to eq("あ")
  end

  it 'can generate sample string of /(?u)[^[^あ]]/' do
    expect(/(?u)[^[^あ]]/.sample).to eq("あ")
  end

  it 'can generate sample string of /(?<![^ab]|[^b])c/' do
    expect(/(?<![^ab]|[^b])c/.sample).to eq("bc")
  end
  
  it 'can generate sample string of /^bb(?<![^b]{2}|[^b])c/' do
    expect(/^bb(?<![^b]{2}|[^b])c/.sample).to eq("bbc")
  end

  it 'can generate sample string of /(?<!(?i:ab))cd/' do
    expect(/(?<!(?i:ab))cd/.match_data).to be_a(MatchData)
  end

end
